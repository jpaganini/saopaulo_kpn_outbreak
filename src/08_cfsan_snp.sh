#!/bin/bash

mkdir ../results/cfsan_results

mkdir ../data/trimmed_reads/BHKPC104
mkdir ../data/trimmed_reads/BHKPC93
mv ../data/trimmed_reads/BHKPC104* ../data/trimmed_reads/BHKPC104
mv ../data/trimmed_reads/BHKPC93* ../data/trimmed_reads/BHKPC93

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate cfsan

cfsan_snp_pipeline run -m soft -c snppipeline.conf -o ../results/cfsan_results/ref_BHKPC104 -s ../data/trimmed_reads ../results/unicycler_assemblies/BHKPC104/assembly.fasta
