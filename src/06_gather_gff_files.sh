#!/bin/bash

#1. Mkdir for the results
mkdir ../results/all_gff_files

#2. Get a list of strains
strains=$(ls ../results/bakta_annotations)

#3. Copy the annotation files
for isolate in $strains
do
cp ../results/bakta_annotations/${isolate}/${isolate}.gff3 ../results/all_gff_files
done
