#! /bin/bash

mkdir ../results/unicycler_assemblies

#2. Get a list of strains - B- CREATE A NEW FILE AND MODIFY THE NAME HERE
files=$(ls ../data/illumina_raw_reads/ | cut -f 1 -d _ | sort -u)

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate unicycler
cd ../results/unicycler_assemblies


for strain in $files
unicycler -1 ../../data/trimmed_reads/${strain}_R1.fq -2 ../../trimmed_reads/${strain}_R2.fq -l ../../data/nanopore_raw_reads/${strain}_nanopore.fastq.gz -o ${strain} -t 8 --mode bold
done

