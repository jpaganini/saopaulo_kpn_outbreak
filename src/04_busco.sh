#!/bin/bash

mkdir ../results/busco_output

conda activate busco

busco -i ../results/unicycler_assemblies/BHKPC104/assembly.fasta -o ../results/busco_output/BHKPC104 -m genome -l bacteria_odb10
busco -i ../results/unicycler_assemblies/BHKPC93/assembly.fasta -o ../results/busco_output/BHKP93 -m genome -l bacteria_odb10

