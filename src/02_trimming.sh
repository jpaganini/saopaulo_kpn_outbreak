#!/bin/bash

mkdir ../data/trimmed_reads


source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate trimming_mmbioit

trim_galore --quality 20 --dont_gzip --length 20 --paired -j 8 --output_dir ../data/trimmed_reads ../data/illumina_raw_reads/*

files=$(ls ../data/trimmed_reads/ | cut -f 1 -d _ | sort -u)

cd ../data/trimmed_reads

#rename the output files
for strains in $files
do
mv ${strains}_R1_val_1.fq ${strains}_R1.fq
mv ${strains}_R2_val_2.fq ${strains}_R2.fq
done

