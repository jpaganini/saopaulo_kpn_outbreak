#!/bin/bash

mkdir ../results/fastqc_output

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate fastqc

fastqc -o ../results/fastqc_output ../data/illumina_raw_reads/*

