#!/bin/bash

#1. mk directory to hold ies
mkdir -p ../results/bakta_annotations

#list genome
genomes=$(ls ../data/illumina_raw_reads/ | cut -f 1 -d _ | sort -u)

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate bakta

for predictions in $genomes
do
bakta --db ../data/databases/bakta/db --prefix ${predictions} --output ../results/bakta_annotations/${predictions} --genus Klebsiella --species pneumoniae --keep-contig-headers --threads 8 ../results/unicycler_assemblies/${predictions}/assembly.fasta
done
