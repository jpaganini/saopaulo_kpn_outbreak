#!/bin/bash

mkdir -p ../results/panaroo_output

source /home/dla_mm/jpaganini/data/miniconda3/etc/profile.d/conda.sh
conda activate panaroo_mmbioit

panaroo -i ../results/all_gff_files/*.gff3 -o ../results/panaroo_output --clean-mode strict --refind_prop_match 0.5 --search_radius 5000 --core_threshold 0.98 -t 8 --remove-invalid-genes

